package model;

public class Item {
    private final int ID,profit,weight,nodeID;
    private final float ratio;

    public Item(int ID, int profit, int weight, int nodeID) {
        this.ID = ID;
        this.profit = profit;
        this.weight = weight;
        this.nodeID = nodeID;
        ratio = profit/(weight*1.0f);
    }

    public int getID() {
        return ID;
    }

    public int getProfit() {
        return profit;
    }

    public int getWeight() {
        return weight;
    }

    public int getNodeID() {
        return nodeID;
    }

    public float getRatio(){
        return ratio;
    }
}
