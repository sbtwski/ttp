package model;

import java.util.ArrayList;
import java.util.Random;

public class Specimen {
    private int[] route;
    private float fitness;
    private final Random generator;

    public Specimen(int[] route) {
        this.route = route;
        generator = new Random();
    }

    public Specimen(Specimen toCopy){
        this.fitness = toCopy.fitness;
        this.generator = new Random();
        this.route = new int[toCopy.route.length];
        System.arraycopy(toCopy.route, 0, this.route, 0, toCopy.route.length);
    }

    public int[] getRoute() {
        return route;
    }

    public float getFitness() {
        return fitness;
    }

    public void setFitness(float fitness) {
        this.fitness = fitness;
    }

    public void mutate(){
        float randomNumber = generator.nextFloat();

        if(randomNumber < Constants.P_M){
            int invStart = generator.nextInt(route.length);
            int invEnd = invStart + generator.nextInt(route.length - invStart);

            int range = invEnd - invStart;
            int temp;

            for(int i = 0; i <= range/2; i++){
                temp = route[invStart + i];
                route[invStart + i] = route[invEnd - i];
                route[invEnd - i] =  temp;
            }
        }
    }

    public Specimen[] crossover(Specimen secondParent){
        float randomNumber = generator.nextFloat();
        Specimen[] result = {this, secondParent};

        if(randomNumber < Constants.P_X){
            int crossStart = generator.nextInt(route.length);
            int crossEnd = crossStart + generator.nextInt(route.length - crossStart);

            Specimen firstOffspring = performPMX(result[0].route, result[1].route, crossStart, crossEnd);
            Specimen secondOffspring = performPMX(result[1].route, result[0].route, crossStart, crossEnd);

            result[0] = new Specimen(firstOffspring);
            result[1] = new Specimen(secondOffspring);
        }

        return result;
    }

    private Specimen performPMX(int[] parent1, int[] parent2, int crossStart, int crossEnd){
        Specimen result = new Specimen(new int[0]);
        int[] crossRoute = new int[parent1.length];

        ArrayList<Integer> nodesToSwap = new ArrayList<>();
        ArrayList<Integer> swapIndices = new ArrayList<>();
        int currentIndex;

        for(int i = crossStart; i <= crossEnd; i++){
            crossRoute[i] = parent1[i];

            currentIndex = indexOfNode(parent1, parent2[i]);
            if(notInRange(currentIndex, crossStart, crossEnd)) {
                nodesToSwap.add(parent1[i]);
                swapIndices.add(i);
            }
        }

        for(int i = 0; i < nodesToSwap.size(); i++){
            currentIndex = indexOfNode(parent2, nodesToSwap.get(i));

            if (notInRange(currentIndex, crossStart, crossEnd))
                crossRoute[currentIndex] = parent2[swapIndices.get(i)];
            else {
                int additionalIndex = indexOfNode(parent2, parent1[currentIndex]);

                while(!notInRange(additionalIndex, crossStart, crossEnd)) {
                    currentIndex = additionalIndex;
                    additionalIndex = indexOfNode(parent2, parent1[currentIndex]);
                }

                crossRoute[additionalIndex] = parent2[swapIndices.get(i)];
            }
        }

        for(int i = 0; i < crossRoute.length; i++){
            if(crossRoute[i] == 0)
                crossRoute[i] = parent2[i];
        }

        result.route = new int[crossRoute.length];
        System.arraycopy(crossRoute,0,result.route,0,crossRoute.length);
        return result;
    }

    private boolean notInRange(int index, int start, int end){
        return index < start || index > end;
    }

    private int indexOfNode(int[] array, int toFind){
        boolean found = false;
        int i = 0;

        while (!found && i < array.length) {
            found = (array[i] == toFind);
            i++;
        }

        return found ? i - 1 : -1;
    }
}
