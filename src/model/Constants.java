package model;

public class Constants {
    public static final int POP_SIZE = 300;
    public static final int T_SIZE = 25;
    public static final int GEN_SIZE = 5000;
    static final float P_X = 0.35f;
    static final float P_M = 0.45f;

    public static final int REPETITION_AMOUNT = 10;

    public static final int PARAMETERS_LOAD_AMOUNT = 5;
    public static final String[] PARAMETERS_LOAD = {"DIMENSION:", "NUMBER OF ITEMS:", "CAPACITY OF KNAPSACK:", "MIN SPEED:", "MAX SPEED:"};
    public static final String NODES_LOAD = "NODE_COORD_SECTION";
    public static final String ITEMS_LOAD = "ITEMS SECTION";
    public static final String EXPORT_HEADER = "Generation;Najlepszy;Średni;Najgorszy";
    public static final String PARAMETERS_EXPORT = ";Pop_size: " + POP_SIZE + ";T_size: " + T_SIZE + ";Gen_size: " + GEN_SIZE + ";Px: " + P_X + ";Pm: " + P_M;

    public static final int BEST_FITNESS_INDEX = 0;
    public static final int WORST_FITNESS_INDEX = 1;
    public static final int AVG_FITNESS_INDEX = 2;

    public static final int TOURNAMENT_INDEX = 0;
    public static final int ROULETTE_INDEX = 1;
    public static final int RANDOM_INDEX = 2;
    public static final int GREEDY_INDEX = 3;

    static final int DIMENSION_INDEX = 0;
    static final int ITEMS_NUMBER_INDEX = 1;
    static final int CAPACITY_INDEX = 2;
    static final int MIN_SPEED_INDEX = 3;
    static final int MAX_SPEED_INDEX = 4;
}
