package model;

public class Node {
    private final int ID,x,y;

    public Node(int ID, int x, int y) {
        this.ID = ID;
        this.x = x;
        this.y = y;
    }

    public int getID() {
        return ID;
    }

    public double calculateDistance(Node secondNode){
        return Math.sqrt(Math.pow(secondNode.x - x, 2) + Math.pow(secondNode.y - y, 2));
    }
}
