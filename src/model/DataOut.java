package model;

public class DataOut {
    private int genID;
    private float bestFit, worstFit, averageFit;

    public DataOut() { }

    public int getGenID() {
        return genID;
    }

    public void setGenID(int genID) {
        this.genID = genID;
    }

    public float getBestFit() {
        return bestFit;
    }

    public float getWorstFit() {
        return worstFit;
    }

    public float getAverageFit() {
        return averageFit;
    }

    public void setStats(float[] stats){
        bestFit = stats[Constants.BEST_FITNESS_INDEX];
        worstFit = stats[Constants.WORST_FITNESS_INDEX];
        averageFit = stats[Constants.AVG_FITNESS_INDEX];
    }
}
