package model;

import java.util.ArrayList;
import java.util.Comparator;

public class DataIn {
    private int nodesAmount, itemsAmount, knapsackCapacity;
    private float maxSpeed, speedWeightRatio;
    private ArrayList<Node> nodes;
    private ArrayList<Item> items;
    private final Comparator<Item> ratioDescending = (Item o1, Item o2) -> Float.compare(o2.getRatio(), o1.getRatio());

    public DataIn() { }

    public int getNodesAmount() {
        return nodesAmount;
    }

    public int getItemsAmount() { return itemsAmount; }

    public int getKnapsackCapacity() {
        return knapsackCapacity;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public float getSpeedWeightRatio() { return speedWeightRatio; }

    public void setParameters(float[] parameters){
        nodesAmount = (int)parameters[Constants.DIMENSION_INDEX];
        itemsAmount = (int)parameters[Constants.ITEMS_NUMBER_INDEX];
        knapsackCapacity = (int)parameters[Constants.CAPACITY_INDEX];
        float minSpeed = parameters[Constants.MIN_SPEED_INDEX];
        maxSpeed = parameters[Constants.MAX_SPEED_INDEX];
        speedWeightRatio = (maxSpeed - minSpeed)/knapsackCapacity;
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public void setNodes(ArrayList<Node> nodes) {
        this.nodes = nodes;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public void sortItemsByRatio(){
        items.sort(ratioDescending);
    }
}
