package io;

import model.Constants;
import model.DataOut;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Exporter {
    private BufferedWriter fileWriter;
    private final String fileName;

    public Exporter(String fileName) {
        this.fileName = fileName;
    }

    public void startWriting(){
        try {
            fileWriter = new BufferedWriter(new FileWriter("./results/"+fileName+".txt"));
            fileWriter.write(Constants.EXPORT_HEADER);
            fileWriter.write(Constants.PARAMETERS_EXPORT);
            fileWriter.newLine();
        }catch (IOException e){
            System.out.println("Cannot create result file");
        }
    }

    public void writeData(DataOut toWrite){
        StringBuilder output = new StringBuilder();

        output.append(toWrite.getGenID());
        output.append(';');
        output.append(convertFloats(toWrite.getBestFit()));
        output.append(';');
        output.append(convertFloats(toWrite.getAverageFit()));
        output.append(';');
        output.append(convertFloats(toWrite.getWorstFit()));

        try {
            fileWriter.write(output.toString());
            fileWriter.newLine();
        }catch (IOException e){
            System.out.println("Cannot write to result file");
        }
    }

    private String convertFloats(Float toConvert){
        String result = String.valueOf(toConvert);
        result = result.replace('.', ',');
        return result;
    }

    public void stopWriting(){
        try {
            fileWriter.close();
        }catch (NullPointerException e){
            System.out.println("FileWriter hasn't been created");
        }catch (IOException e){
            System.out.println("File cannot be closed");
        }
    }
}
