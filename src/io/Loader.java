package io;

import model.Constants;
import model.DataIn;
import model.Item;
import model.Node;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Loader {
    private final File dataFile;
    private Scanner fileScanner;

    public Loader(String fileName) {
        dataFile = new File("./src/data/"+fileName+".ttp");
    }

    public DataIn readFile(){
        DataIn result = new DataIn();

        try {
            fileScanner = new Scanner(dataFile);

            result.setParameters(readParameters());
            result.setNodes(readNodes());
            result.setItems(readItems());

            fileScanner.close();
        }catch (FileNotFoundException e){
            System.out.println("File not found!");
        }

        return result;
    }

    private float[] readParameters(){
        float[] result = new float[Constants.PARAMETERS_LOAD_AMOUNT];
        String currentLine = fileScanner.nextLine();
        int parameterIndex = 0;

        while(!currentLine.contains(Constants.NODES_LOAD)){
            if(parameterIndex < Constants.PARAMETERS_LOAD_AMOUNT && currentLine.contains(Constants.PARAMETERS_LOAD[parameterIndex])){
                currentLine = currentLine.replace(Constants.PARAMETERS_LOAD[parameterIndex], "");
                currentLine = currentLine.trim();
                result[parameterIndex] = Float.parseFloat(currentLine);
                parameterIndex++;
            }
            currentLine = fileScanner.nextLine();
        }

        return result;
    }

    private ArrayList<Node> readNodes(){
        ArrayList<Node> result = new ArrayList<>();
        String currentLine = fileScanner.nextLine();
        String[] splitted;
        int currentID, currentX, currentY;

        while (!currentLine.contains(Constants.ITEMS_LOAD)){
            splitted = currentLine.split("\\s+");

            if(splitted.length == 3) {
                currentID = Integer.parseInt(splitted[0]);
                currentX = (int)Float.parseFloat(splitted[1]);
                currentY = (int)Float.parseFloat(splitted[2]);

                result.add(new Node(currentID, currentX, currentY));
            }
            currentLine = fileScanner.nextLine();
        }

        return result;
    }

    private ArrayList<Item> readItems(){
        ArrayList<Item> result = new ArrayList<>();
        String currentLine = fileScanner.nextLine();
        String[] splitted;
        int currentID, currentProfit, currentWeight, currentNodeID;

        while (!currentLine.isEmpty()){
            splitted = currentLine.split("\\s+");

            if(splitted.length == 4) {
                currentID = Integer.parseInt(splitted[0]);
                currentProfit = Integer.parseInt(splitted[1]);
                currentWeight = Integer.parseInt(splitted[2]);
                currentNodeID = Integer.parseInt(splitted[3]);

                result.add(new Item(currentID, currentProfit, currentWeight, currentNodeID));
            }

            if(fileScanner.hasNextLine())
                currentLine = fileScanner.nextLine();
            else
                currentLine = "";
        }

        return result;
    }
}
