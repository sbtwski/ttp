import management.GeneralManagement;
import model.Constants;

import java.util.Scanner;

class Main {

    public static void main(String[] args){
        String fileIn, fileOut;
        Scanner userInput = new Scanner(System.in);

        System.out.print("Enter source file name: ");
        fileIn = userInput.next();

        System.out.println("1. GA with full data export");
        System.out.println("2. Comparison between algorithms");
        System.out.print("Select mode: ");

        int selection = userInput.nextInt();

        switch (selection) {
            case 1:
                System.out.print("Enter export file name: ");
                fileOut = userInput.next();
                gaAnalysis(fileIn, fileOut);
                break;
            case 2:
                algorithmsComparison(fileIn);
                break;
        }

        userInput.close();
    }

    private static void gaAnalysis(String fileIn, String fileOut){
        GeneralManagement gaManager = new GeneralManagement(fileIn, true);
        System.out.println("Computation started");
        long startTime = System.nanoTime();
        gaManager.geneticAlgorithm();
        System.out.println("Computation finished");
        gaManager.exportResults(fileOut);
        System.out.println("Results exported to file "+fileOut+".txt");
        System.out.println("Computation time was " + (System.nanoTime() - startTime) / Math.pow(10,9) + " seconds");
    }

    private static void algorithmsComparison(String fileIn){
        GeneralManagement gaManager;
        System.out.println("Computation started");

        long startTime = System.nanoTime();
        float[] stats = new float[Constants.REPETITION_AMOUNT];
        float average;
        double standardDeviation;
        String[] versions = {"Genetic algorithm with tournament", "Genetic algorithm with roulette", "Random algorithm", "Greedy algorithm"};

        for(int k=0; k < versions.length; k++){
            long currentStartTime = System.nanoTime();
            for(int i=0; i < Constants.REPETITION_AMOUNT; i++){
                gaManager = new GeneralManagement(fileIn,true);

                switch (k){
                    case Constants.TOURNAMENT_INDEX:
                        gaManager.geneticAlgorithm();
                        break;
                    case Constants.ROULETTE_INDEX:
                        gaManager.switchToRoulette();
                        gaManager.geneticAlgorithm();
                        break;
                    case Constants.RANDOM_INDEX:
                        gaManager.randomAlgorithm();
                        break;
                    case Constants.GREEDY_INDEX:
                        gaManager.greedyAlgorithm();
                        break;
                }

                stats[i] = gaManager.getResult().getBestFit();
            }

            average = calculateAverage(stats);
            standardDeviation = calculateStandardDeviation(stats, average);
            printStats(versions[k], average, standardDeviation);

            System.out.println("Method computation time was " + (System.nanoTime() - currentStartTime) / Math.pow(10,9) + " seconds");
        }

        System.out.println();
        System.out.println("Full computation time was " + (System.nanoTime() - startTime) / Math.pow(10,9) + " seconds");
    }

    private static float calculateAverage(float[] stats){
        float result = 0;

        for(float current: stats)
            result += current;

        return result/(stats.length*1.0f);
    }

    private static double calculateStandardDeviation(float[] stats, float avg){
        double result = 0;

        for(float current: stats){
            result += Math.pow(current - avg, 2);
        }

        result /= (stats.length*1.0);

        return Math.sqrt(result);
    }

    private static void printStats(String name, float avg, double stdDev){
        System.out.println();
        System.out.println(name+" statistics:");

        System.out.printf("Average: %.2f \n",avg);
        System.out.printf("Standard deviation: %.2f \n",stdDev);
    }
}
