package management;

import model.Constants;
import model.Node;
import model.Specimen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

class PopulationManagement {
    private Specimen[] population;
    private final Random generator;
    private final int nodesAmount;
    private boolean tournamentVersion;
    private final Comparator<Specimen> fitnessRank = (Specimen s1, Specimen s2) -> Float.compare(s2.getFitness(), s1.getFitness());

    PopulationManagement(int nodesAmount, boolean tournamentVersion) {
        population = new Specimen[Constants.POP_SIZE];
        generator = new Random();
        this.nodesAmount = nodesAmount;
        this.tournamentVersion = tournamentVersion;
    }

    void rouletteVersion() {
        tournamentVersion = false;
    }

    Specimen[] getPopulation() {
        return population;
    }

    void initializePopulation(){
        int[] nodeIDs = new int[nodesAmount];
        int[] currentRoute;
        int selectedPosition, k, effectiveSize;

        for(int i = 0; i < Constants.POP_SIZE; i++){
            for(int j = 1; j <= nodesAmount; j++)
                nodeIDs[j-1] = j;

            currentRoute = new int[nodesAmount];
            k = 0;
            effectiveSize = nodesAmount;

            while (effectiveSize > 0){
                selectedPosition = generator.nextInt(effectiveSize);
                currentRoute[k] = nodeIDs[selectedPosition];
                k++;

                effectiveSize--;
                nodeIDs[selectedPosition] = nodeIDs[effectiveSize];
            }

            population[i] = new Specimen(currentRoute);
        }
    }

    float[] getGenerationStats(){
        float[] stats = new float[3];
        float bestFitness = population[0].getFitness();
        float worstFitness = population[0].getFitness();
        float fitnessSum = population[0].getFitness();

        for(int i = 1; i < population.length; i++){
            float currentFitness = population[i].getFitness();
            fitnessSum += currentFitness;

            if(currentFitness < worstFitness)
                worstFitness = currentFitness;
            else if(currentFitness > bestFitness)
                bestFitness = currentFitness;
        }

        stats[Constants.BEST_FITNESS_INDEX] = bestFitness;
        stats[Constants.WORST_FITNESS_INDEX] = worstFitness;
        stats[Constants.AVG_FITNESS_INDEX] = fitnessSum/population.length;

        return stats;
    }

    void createNewPopulation(){
        Specimen[] selected = new Specimen[2];
        int added = 0;

        while(added < Constants.POP_SIZE){
            selected[0] = tournamentVersion ? tournament() : roulette();
            selected[1] = tournamentVersion ? tournament() : roulette();

            selected = selected[0].crossover(selected[1]);

            selected[0].mutate();
            selected[1].mutate();

            population[added] = new Specimen(selected[0]);
            added++;
            population[added] = new Specimen(selected[1]);
            added++;
        }
    }

    @SuppressWarnings("unchecked")
    void greedySolution(ArrayList<Node> sourceNodes){
        population = new Specimen[1];
        ArrayList<Node> nodes = (ArrayList<Node>)sourceNodes.clone();

        int startingNode = generator.nextInt(nodesAmount);
        int[] route = new int[nodesAmount];

        Node currentNode = nodes.get(startingNode);
        Node bestNode;
        double bestDistance, currentDistance;

        route[0] = currentNode.getID();

        for(int i = 1; i < nodesAmount; i++){
            nodes.remove(currentNode);

            bestNode = nodes.get(0);
            bestDistance = currentNode.calculateDistance(bestNode);

            for(int j = 1; j < nodes.size(); j++){
                currentDistance = currentNode.calculateDistance(nodes.get(j));
                if(currentDistance < bestDistance){
                    bestNode = nodes.get(j);
                    bestDistance = currentDistance;
                }
            }
            route[i] = bestNode.getID();
            currentNode = bestNode;
        }

        Specimen greedyRoute = new Specimen(route);
        population[0] = greedyRoute;
    }

    private Specimen tournament(){
        int[] chooseFrom = new int[population.length];
        Specimen[] participants = new Specimen[Constants.T_SIZE];
        int position;
        int effectiveSize = chooseFrom.length;

        for(int i = 0; i < population.length; i++)
            chooseFrom[i] = i;

        for(int i = 0; i < Constants.T_SIZE; i++){
            position = generator.nextInt(effectiveSize);
            participants[i] = population[chooseFrom[position]];

            effectiveSize--;
            chooseFrom[position] = chooseFrom[effectiveSize];
        }

        float bestFitness = participants[0].getFitness();
        position = 0;
        for(int i = 1; i < Constants.T_SIZE; i++){
            if(participants[i].getFitness() > bestFitness){
                bestFitness = participants[i].getFitness();
                position = i;
            }
        }

        return new Specimen(participants[position]);
    }

    private Specimen roulette(){
        float fitnessSum = 0;

        ArrayList<Specimen> ranking = new ArrayList<>(Arrays.asList(population));
        ranking.sort(fitnessRank);

        float offset = Math.abs(ranking.get(ranking.size()-1).getFitness());

        for(Specimen current: ranking){
            current.setFitness(current.getFitness() + offset);
            fitnessSum += current.getFitness();
        }

        float[] probabilities = new float[ranking.size()];

        for(int i = 1; i < probabilities.length; i++)
            probabilities[i] = probabilities[i-1] + (ranking.get(i-1).getFitness()/fitnessSum);

        float draw = generator.nextFloat();
        boolean found = false;
        int i = 0;

        while (!found && i < probabilities.length - 1){
            i++;
            if(draw <= probabilities[i])
                found = true;

        }

        return new Specimen(ranking.get(i-1));
    }
}
