package management;

import io.Exporter;
import io.Loader;
import model.*;

import java.util.ArrayList;
import java.util.HashMap;

public class GeneralManagement {
    private final DataIn sourceData;
    private final PopulationManagement popManager;
    private final ArrayList<DataOut> results;
    private final HashMap<Integer, Integer> greedyKnapsack;
    private final ArrayList<Node> nodes;
    private int profit;
    private final float maxSpeed, speedWeightRatio;

    public GeneralManagement(String fileName, boolean tournament) {
        Loader fileLoader = new Loader(fileName);
        sourceData = fileLoader.readFile();
        sourceData.sortItemsByRatio();
        maxSpeed = sourceData.getMaxSpeed();
        speedWeightRatio = sourceData.getSpeedWeightRatio();

        popManager = new PopulationManagement(sourceData.getNodesAmount(), tournament);
        results = new ArrayList<>(Constants.GEN_SIZE);
        greedyKnapsack = new HashMap<>();
        nodes = sourceData.getNodes();
    }

    public void exportResults(String fileName){
        Exporter resultsExport = new Exporter(fileName);
        resultsExport.startWriting();

        for(DataOut toWrite: results)
            resultsExport.writeData(toWrite);

        resultsExport.stopWriting();
    }

    public void switchToRoulette(){
        popManager.rouletteVersion();
    }

    public DataOut getResult(){
        return results.get(results.size()-1);
    }

    public void geneticAlgorithm(){
        popManager.initializePopulation();
        prepareKnapsack();

        for(int i = 0; i < Constants.GEN_SIZE; i++){
            evaluate();
            generationLog(i);
            popManager.createNewPopulation();
        }
    }

    public void randomAlgorithm(){
        popManager.initializePopulation();
        prepareKnapsack();
        evaluate();
        generationLog(0);
    }

    public void greedyAlgorithm(){
        popManager.greedySolution(nodes);
        prepareKnapsack();
        evaluate();
        generationLog(0);
    }

    private void prepareKnapsack(){
        ArrayList<Item> availableItems = sourceData.getItems();
        int capacity = sourceData.getKnapsackCapacity();
        Item currentItem;
        int position = 0;
        int nodeWeight;
        profit = 0;

        for(Node current: nodes)
            greedyKnapsack.put(current.getID(), 0);

        while (capacity > 0 && position < availableItems.size()){
            currentItem = availableItems.get(position);

            if(capacity - currentItem.getWeight() >= 0){
                capacity -= currentItem.getWeight();
                profit += currentItem.getProfit();

                nodeWeight = greedyKnapsack.get(currentItem.getNodeID()) + currentItem.getWeight();
                greedyKnapsack.put(currentItem.getNodeID(), nodeWeight);
                availableItems.remove(position);
            }
            else
                position++;
        }
    }

    private void evaluate(){
        float fitnessResult, travelTime;
        double distance;
        int currentWeight;
        int[] route;
        Node firstNode, secondNode;

        for(Specimen specimen: popManager.getPopulation()){
            fitnessResult = profit;
            travelTime = 0;
            currentWeight = 0;

            route = specimen.getRoute();

            for(int i = 0; i < route.length - 1; i++){
                firstNode = nodes.get(route[i]-1);
                secondNode = nodes.get(route[i+1]-1);

                distance = firstNode.calculateDistance(secondNode);
                currentWeight += greedyKnapsack.get(firstNode.getID());

                travelTime += distance / (maxSpeed - (currentWeight * speedWeightRatio));
            }

            firstNode = nodes.get(route[route.length-1]-1);
            secondNode = nodes.get(0);

            distance = firstNode.calculateDistance(secondNode);
            currentWeight += greedyKnapsack.get(firstNode.getID());

            travelTime += distance / (maxSpeed - (currentWeight * speedWeightRatio));

            fitnessResult -= travelTime;
            specimen.setFitness(fitnessResult);
        }
    }

    private void generationLog(int genID){
        DataOut currentGen = new DataOut();
        currentGen.setGenID(genID);
        float[] stats = popManager.getGenerationStats();
        currentGen.setStats(stats);
        results.add(currentGen);
    }
}
